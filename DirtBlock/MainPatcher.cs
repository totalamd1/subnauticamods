﻿using System.Reflection;
using SMLHelper.V2.Handlers;
using Harmony;
using UnityEngine;
using System;
using SMLHelper.V2.Options;
using System.Collections.Generic;
using System.IO;
using Oculus.Newtonsoft.Json;
using Oculus.Newtonsoft.Json.Linq;

namespace DirtBlock{
    public static class MainPatcher {

        public static AssetBundle assetBundle;

        public static void Patch()
        {

            var path = @"./QMods/DirtBlock/Assets/dirtblock";

            assetBundle = AssetBundle.LoadFromFile(path);

            if (assetBundle == null)
            {
                Console.WriteLine("ERROR: DirtBlock: Cannot locate asset bundle");
            }

            var dirtBlock = new DirtBlock();
            var randomBlock = new RandomBlock();
            var snapBlock = new SnapBlock();

            dirtBlock.Patch();
            randomBlock.Patch();
            snapBlock.Patch();

            var harmony = HarmonyInstance.Create("com.oldark.subnautica.dirtblock.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());

            OptionsPanelHandler.RegisterModOptions(new Options("Dirt Block"));
        }
    }

    public class Options : ModOptions {
        public Options(string name) : base(name)
        {
            try
            {
                ToggleChanged += OnToggleChanged;
            }
            catch(Exception e)
            {

            }
        }

        public override void BuildModOptions()
        {
            InitializeConfig();

            try
            {
                AddToggleOption("useVibrantColors", "Use Vibrant Colors", DirtBlockConfig.useVibrantColors);
                AddToggleOption("allBlocksSnap", "Enable all blocks to snap", DirtBlockConfig.useVibrantColors);
            }
            catch(Exception e)
            {
                
            }
        }

        public void OnToggleChanged(object sender, ToggleChangedEventArgs args)
        {
            switch (args.Id)
            {
                case "useVibrantColors":
                    DirtBlockConfig.useVibrantColors = args.Value;
                    break;
                case "allBlocksSnap":
                    DirtBlockConfig.allBlocksSnap = args.Value;
                    break;
            }
            
        }

        void UpdatePlayerPrefs()
        {
            if (DirtBlockConfig.useVibrantColors)
            {
                PlayerPrefs.SetInt("DirtBlock_useVibrantColors", 1);
            }
            else
            {
                PlayerPrefs.SetInt("DirtBlock_useVibrantColors", 0);
            }

            if (DirtBlockConfig.allBlocksSnap)
            {
                PlayerPrefs.SetInt("DirtBlock_allBlocksSnap", 1);
            }
            else
            {
                PlayerPrefs.SetInt("DirtBlock_allBlocksSnap", 0);
            }
            PlayerPrefs.Save();
        }

        // Read previous option choices from player prefs to set defaults
        void InitializeConfig()
        {
            // Read values from playerprefs
            if (PlayerPrefs.HasKey("DirtBlock_useVibrantColors"))
            {
                if(PlayerPrefs.GetInt("DirtBlock_useVibrantColors") == 0)
                {
                    DirtBlockConfig.useVibrantColors = false;
                }
                else
                {
                    DirtBlockConfig.useVibrantColors = true;
                }
            }
            if (PlayerPrefs.HasKey("DirtBlock_allBlocksSnap"))
            {
                if (PlayerPrefs.GetInt("DirtBlock_allBlocksSnap") == 0)
                {
                    DirtBlockConfig.allBlocksSnap = false;
                }
                else
                {
                    DirtBlockConfig.allBlocksSnap = true;
                }
            }

            // Initialize config modded block list
            // @TODO
            var path = @"./QMods/DirtBlock/mod.json";
            JObject modjson;
            using (StreamReader file = File.OpenText(path))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                modjson = (JObject)JToken.ReadFrom(reader);
            }
            

            //DirtBlockConfig.snappingBlocks.Add("DirtBlock");
            DirtBlockConfig.snappingBlocks.Add("RandomBlock");
            DirtBlockConfig.snappingBlocks.Add("SnapBlock");
        }
    }

    public class DirtBlockConfig : MonoBehaviour {
        public static bool useVibrantColors = false;
        public static bool allBlocksSnap = false;

        public static List<string> snappingBlocks = new List<string>();

    }
}