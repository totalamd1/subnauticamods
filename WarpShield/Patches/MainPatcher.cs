﻿using Harmony;
using System.Reflection;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Options;
using UnityEngine;
using System;

namespace MAC.WarpShield {
    public class MainPatcher {

        public static bool isSeamothWarpShielded = false;
        public static bool isPrawnWarpShielded = false;
        public static bool isWarperSlain = false;

        public static void Patch()
        {
            WarpShieldModule.PatchSMLHelper();

            var harmony = HarmonyInstance.Create("com.oldark.subnautica.warpshield.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
            OptionsPanelHandler.RegisterModOptions(new Options("Warpshield"));


        }
    }

    public class Options : ModOptions {
        public Options(string name) : base(name)
        {
            try
            {
                ToggleChanged += OnToggleChanged;
            }
            catch(Exception e)
            {

            }
        }

        public override void BuildModOptions()
        {
            InitializeConfig();

            try
            {
                AddToggleOption("warpshield_requirekill", "Require Kill", WarpShieldConfig.requireKill);
            }
            catch (Exception e)
            {
                
            }
        }

        public void OnToggleChanged(object sender, ToggleChangedEventArgs e)
        {
            try
            {
                if (e.Id == "warpshield_requirekill")
                {
                    WarpShieldConfig.requireKill = e.Value;
                }
                UpdatePlayerPrefs();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: WarpShield onToggleChanged for " + e.Id);
            }
        }

        void UpdatePlayerPrefs()
        {
            if (WarpShieldConfig.requireKill)
            {
                PlayerPrefs.SetInt("warpshield_requireKill", 1);
            }
            else
            {
                PlayerPrefs.SetInt("warpshield_requireKill", 0);
            }
            
            PlayerPrefs.Save();
        }

        void InitializeConfig()
        {
            if (PlayerPrefs.HasKey("warpshield_requireKill"))
            {
                if (PlayerPrefs.GetInt("warpshield_requireKill") == 0)
                {
                    WarpShieldConfig.requireKill = false;
                }
                else
                {
                    WarpShieldConfig.requireKill = true;
                }
            }
        }
    }

    public class WarpShieldConfig : MonoBehaviour {
        public static bool requireKill = true;
    }
}