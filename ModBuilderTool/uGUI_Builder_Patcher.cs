﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harmony;
using UnityEngine;
using System.Reflection;

namespace ModBuilderTool {

    [HarmonyPatch(typeof(uGUI_BuilderMenu))]
    [HarmonyPatch("UpdateItems")]
    public class uGUI_BuilderMenu_UpdateItems_Patch {

        [HarmonyPrefix]
        static bool Prefix(uGUI_BuilderMenu __instance)
        {
            Console.WriteLine("YOLO held tool: " + Inventory.main.GetHeldTool().name);
            if(Inventory.main.GetHeldTool().Equals("ModBuilderTool(Clone)"))
            {
                Console.WriteLine("YOLO: updating items in menu for modded tool");

                Dictionary<string, TechType> items;
                FieldInfo reflectionInfo = typeof(uGUI_BuilderMenu).GetField("items");
                object obj = reflectionInfo.GetValue(null);
                items = (Dictionary<string, TechType>)obj;

                items.Clear();
                Console.WriteLine("YOLO - there should be no more items");
                return false;
            }
            return true;

        }
    }
}