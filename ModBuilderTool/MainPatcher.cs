﻿using Harmony;
using System.Reflection;
using UnityEngine;
using System;
using SMLHelper.V2.Options;
using SMLHelper.V2.Handlers;

namespace ModBuilderTool
{
    public class MainPatcher
    {

        public static void Patch()
        {
            ModBuilderTool.PatchSMLHelper();
            var harmony = HarmonyInstance.Create("com.oldark.subnautica.modbuildertool.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }

    }
}
