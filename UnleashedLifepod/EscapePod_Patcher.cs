﻿using Harmony;
using System;
using UnityEngine;

namespace Unleashed_Lifepod
{

    [HarmonyPatch(typeof(EscapePod))]
    [HarmonyPatch("StartAtPosition")]
    internal class EscapePod_StartAtPosition_Patch
    {
        [HarmonyPrefix]
        public static bool Prefix(ref Vector3 position,  EscapePod __instance)
        {
            __instance.transform.position = position;
            __instance.anchorPosition = position;
            __instance.RespawnPlayer();

            return false;
        }

    }

    [HarmonyPatch(typeof(EscapePod))]
    [HarmonyPatch("FixedUpdate")]
    internal class EscapePod_FixedUpdate_Patch {

        static Transform podOriginalTransform;

        [HarmonyPrefix]
        public static bool Prefix(EscapePod __instance)
        {
            podOriginalTransform = __instance.transform;
            WorldForces wf = __instance.GetComponent<WorldForces>();
            if (wf.aboveWaterGravity == 50f && Ocean.main.GetDepthOf(__instance.gameObject) > 0 )
            {
                wf.aboveWaterGravity = 9.81f;
            }
            return true;
        }

        [HarmonyPostfix]
        public static void Postfix(EscapePod __instance)
        {
            if(MainPatcher.podAnchored && UnleashedLifepodConfig.heavyPod)
            {
                __instance.transform.position = podOriginalTransform.position;
            }
        }
    }

    [HarmonyPatch(typeof(EscapePod))]
    [HarmonyPatch("Awake")]
    internal class EscapePod_Awake_Patch {
        public static Shader originalLifePodShader;
        //public static GameObject yoloGO;

        [HarmonyPostfix]
        public static void Postfix()
        {
            EscapePod pod = EscapePod.main;

            if (UnleashedLifepodConfig.allowControl)
            {
                foreach (var renderer in pod.GetComponentsInChildren<SkinnedMeshRenderer>())
                {
                    String gameObjectName = renderer.gameObject.name;
                    Color color = renderer.material.color;
                    originalLifePodShader = renderer.material.shader;
                    switch (gameObjectName)
                    {
                        // For the record, this makes it transparent from the outside
                        //case "life_pod_damaged":
                        //    Console.WriteLine("YOLO - making things sneaky : " + renderer.gameObject.name);
                        //    renderer.material.shader = UnityEngine.Shader.Find("Transparent/Diffuse");
                        //    renderer.material.color = new Color(252, 61, 3, 1.0f;
                        //    break;
                        case "life_pod_interior_damaged1":
                            renderer.material.shader = UnityEngine.Shader.Find("Transparent/Diffuse");
                            renderer.material.color = new Color(color.r, color.g, color.b, 0.7f);

                            break;
                    }
                }
            }

            WorldForces wf = pod.GetComponent<WorldForces>();
            wf.underwaterGravity = 0.0f;

            if(UnleashedLifepodConfig.heavyPod == true)
            {
                wf.underwaterGravity = 9.81f;
            }
            
            wf.aboveWaterGravity = 50f;

            //Parachute chute = new Parachute();
            //chute.GetPrefabs();
            //yoloGO = chute.GetGameObject();

        }
    }


    //[HarmonyPatch(typeof(EscapePod))]
    //[HarmonyPatch("Update")]
    //internal class EscapePod_Awake_Patch
    //{
    //    [HarmonyPostfix]
    //    public static void Postfix(EscapePod __instance)
    //    {
    //        EscapePod pod = EscapePod.main;
    //        // Setup beacon to use for some persistance
    //        Beacon podData = pod.gameObject.GetComponent<Beacon>();
    //        if (podData != null)
    //        {
    //            //Existing beacon found. Check it's label and sync it up with the Unleashed static flag for anchoring.
    //            Console.WriteLine("YOLO Found existing Beacon component: " + podData.beaconLabel.GetLabel());
    //            if (podData.beaconLabel.GetLabel().Equals("anchored"))
    //            {
    //                MainPatcher.podAnchored = true;
    //            }
    //            else if (podData.beaconLabel.GetLabel().Equals("not-anchored"))
    //            {
    //                MainPatcher.podAnchored = false;
    //            }
    //            else
    //            {
    //                if (MainPatcher.podAnchored)
    //                {
    //                    podData.beaconLabel.SetLabel("anchored");
    //                }
    //                else
    //                {
    //                    podData.beaconLabel.SetLabel("not-anchored");
    //                }
    //                string podDataString = podData.beaconLabel.GetLabel();
    //                Console.WriteLine("YOLO Beacon string is: " + podDataString);
    //            }
    //        }
    //    }
    //}
    //internal class Parachute {

    //    private AssetBundle assetBundle;
    //    private GameObject _Prefab;
    //    private string prefabName = "unitychan";

    //    public GameObject GetGameObject()
    //    {
    //        // new Vector3(0.3f,1.3f,-1.2f)
    //        var prefab = GameObject.Instantiate(_Prefab, EscapePod.main.transform.position + new Vector3(0f,0f,10.2f), Quaternion.identity, EscapePod.main.transform);

    //        if(prefab != null)
    //        {
    //            Console.WriteLine("UnleashedLifepod: Found custom prefab");
    //        }
    //        else
    //        {
    //            Console.WriteLine("ERROR: UnleashedLifepod: Cannot find custom prefab");
    //        }
    //        return prefab;
    //    }

    //    public bool GetPrefabs()
    //    {
    //        var path = @"./QMods/UnleashedLifepod/advTestBundle";
    //        assetBundle = AssetBundle.LoadFromFile(path);
    //        if(assetBundle == null)
    //        {
    //            Console.WriteLine("ERROR: UnleashedLifepod: Unable to locate assetbundle file.");
    //            return false;
    //        }

    //        GameObject prefab = assetBundle.LoadAsset<GameObject>(prefabName);
    //        if(prefab != null)
    //        {
    //            _Prefab = prefab;

    //            Shader shader = Shader.Find("MarmosetUBER");
    //            Renderer[] renderers = prefab.GetComponentsInChildren<Renderer>();

    //            foreach (Renderer renderer in renderers)
    //            {
    //                renderer.material.shader = shader;
    //            }
    //            return true;
    //        }
    //        Console.WriteLine("ERROR: UnleashedLifepod: Prefab not found, " + prefabName);
    //        return false;
    //    }
    //}


}
