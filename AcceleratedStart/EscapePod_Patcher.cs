﻿using Harmony;
using System;
using UnityEngine;

namespace AcceleratedStart
{
    [HarmonyPatch(typeof(EscapePod))]
    [HarmonyPatch("DamageRadio")]
    internal class EscapePod_DamageRadio_Patcher
    {
        [HarmonyPrefix]
        public static bool Prefix(EscapePod __instance)
        {
            var path = @"./QMods/AcceleratedStart/config.txt";
            string line;

            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            line = line.Substring(13);

            if (line == "True" || line == "true" || line == "TRUE")
            {
                return false; //overwrites the vanilla damageRadio method with doing nothing.
            }
            else
            {
                return true; //carry on as usual and break the damned thing
            }
            
        }
    }

    [HarmonyPatch(typeof(EscapePod))]
    [HarmonyPatch("StopIntroCinematic")]
    internal class EscapePod_StopIntroCinematic_Patcher
    {
        [HarmonyPrefix]
        public static bool Prefix(EscapePod __instance)
        {
            var path = @"./QMods/AcceleratedStart/config.txt";
            string line;

            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            line = file.ReadLine();
            line = line.Substring(13);
            if (line == "True" || line == "true" || line == "TRUE")
            {
                __instance.liveMixin.ResetHealth(); // sets the escape pod's damage value back to 0.
            }
            file.Close();
 
            return true;
        }
        [HarmonyPostfix]
        public static void Postfix(EscapePod __instance)
        {
            Player.main.liveMixin.ResetHealth();   // return the player to full health
            __instance.UpdateDamagedEffects();     // causes the sparks and damaged appearance to go away
        }


    }
}
