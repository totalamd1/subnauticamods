﻿using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GentleReapers {
    [HarmonyPatch(typeof(EntitySlot))]
    [HarmonyPatch("Start")]
    internal class EntitySlot_Start_Patch {
        [HarmonyPostfix]
        public static void Postfix(EntitySlot __instance)
        {
            Console.WriteLine("YOLO - BiomeType: " + __instance.GetBiomeType());

        }
    }
}
