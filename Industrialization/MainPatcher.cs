﻿using System.Reflection;
using SMLHelper.V2.Handlers;
using Harmony;
using UnityEngine;
using System;

namespace Industrialization {
    public static class MainPatcher {
        public static void Patch()
        {
            var miningStation = new MiningStation();

            miningStation.Patch();

            var harmony = HarmonyInstance.Create("com.oldark.subnautica.industrialization.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
            
        }

    }
}